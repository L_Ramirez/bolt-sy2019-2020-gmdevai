﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehicleMovement : MonoBehaviour
{
    public Transform goal;
    public float speed = 0;
    public float rotationSpeed = 5;
    public float Accel = 5;
    public float Deccel = 5;
    public float Minspeed =0;
    public float Maxspeed = 10;
    public float brakeAngle = 20;


    void Start()
    {
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        Vector3 lookAtGoal = new Vector3(goal.position.x, this.transform.position.y, goal.position.z);

        Vector3 direction = lookAtGoal - this.transform.position;

        this.transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation(direction), Time.deltaTime * rotationSpeed);

        if (Vector3.Angle(goal.forward, this.transform.forward) > brakeAngle && speed > 2)
        {
            speed = Mathf.Clamp(speed - (Deccel * Time.deltaTime), Minspeed, Maxspeed);
        }
        else
        {
            speed = Mathf.Clamp(speed + (Accel * Time.deltaTime), Minspeed, Maxspeed);
        }


        this.transform.Translate(0, 0, speed);
    }
}
